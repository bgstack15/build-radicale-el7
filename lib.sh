#!/bin/sh
# File: lib.sh
# Author: bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2022-05-17
# Title: Library for build-radicale-centos7
# Project: build-radicale-centos7
# Purpose: library of common functions and variables
# History:
# Usage: dot-sourced by other scripts here
# Reference:
# Improve:
# Dependencies:
# Documentation:
#    README.md

WORKDIR=~/dev2
FEDORA_VER=f36
# COPR username comes from ~/.config/copr auth file
COPR_REPO=radicale-el7

get_latest() {
   # call: get_latest ~/rpmbuild/SRPMS "python-nose*.src.rpm"
   # returns: most-recently-modified python-nose*.src.rpm file
   ___gl_path="${1}"
   ___gl_pattern="${2}"
   (
      cd "${___gl_path}" 
      basename $( find . -iname "${___gl_pattern}" -exec ls -1t {} + 2>/dev/null | head -n1 ; )
   )
}

# README for build-radicale-el7 project
<!--
File: README.md
Locations:
   https://gitlab.com/bgstack15/build-radicale-el7
   https://bgstack15.ddns.net/cgit/build-radicale-el7
Author: bgstack15
Startdate: 2022-05-17
SPDX-License-Identifier: GPL-3.0
Title: README for build-radicale-el7 project
Purpose: Describe the project
History:
Usage:
Reference: See References heading
Improve:
Dependencies: See Dependencies heading
Documentation: This whole file
  -->

## Overview
This repository contains the steps required to build Radicale3 for CentOS 7 and put the rpms in a copr. The process is generally, "adapt the Fedora packages," which works very well!

## Dependencies

### Packages

* rpmbuild
* git

### Git repositories

* https://src.fedoraproject.org/rpms/python-nose
* https://src.fedoraproject.org/rpms/python-passlib
* https://src.fedoraproject.org/rpms/python-dateutil
* https://src.fedoraproject.org/rpms/python-vobject
* https://src.fedoraproject.org/rpms/radicale
* https://gitlab.com/bgstack15/radicale_auth_ldap
* https://gitlab.com/bgstack15/radicaleInfCloud

## Using
You need to establish a [COPR](https://copr.fedorainfracloud.org/) project, and also an api key. Store the api key in ~/.config/copr as typical for copr.

Clone this repository, and examine the shell scripts, at least the top parts, to confirm the variables it will use.

When ready, run `./1-build-srpms.sh`. It will generate in the typical ~/rpmbuild/SRPMS/ path the customized srpms. Some needed no modification from the Fedora sources. Some needed minor changes like `python3` -> `python36`, and some needed 

It is possible that my build environment, in which I had already run everything manually, already had the Source: files which tend to be listed in the `sources` file for Fedora's dist-git process. You might have to manually fetch source tarballs. Give `spectool -R -g *.spec` a try in each directory as needed, otherwise you will have to visit each upstream and download the appropriate tarball.

With all the srpms available, run `./2-upload-srpms.sh`. It will run `copr-cli` in the correct order for the build dependencies of the packages. It is not incredibly robust; if any package fails you will have to address it and then re-run the whole thing. I need to redesign it to accept parameters. Pull requests welcome.

Additional packages of type **rpkg** in my COPR include:

Package      | clone url                                                | committish
------------------ | -------------------------------------------------- | ----------
radicale_auth_ldap | https://gitlab.com/bgstack15/radicale_auth_ldap    | 3
infcloud           | https://gitlab.com/bgstack15/radicaleInfCloud      | stackrpms

Package `radicale_auth_ldap` is a bonus: I don't actually use it, but I patched it when testing it before going in a different direction (authentication at the reverse proxy level).

Package `infcloud` is the web front-end for the calendar and address book. Its documentation demonstrates how to set up my reverse-proxy with ldap authentication.

### My modifications
This project modifies Radicale3 to automatically log in the user at the web interface, based on the passed header `HTTP_X_REMOTE_USER`. This might be not to your liking, so you should poke around and disable it if you do not like that.

## Reason for existing
Radicale is CentOS 7 is version 1.1.2, which is out of date. As of the time of this writing, Radicale is at version 3.1.7. These steps allow me to use Radicale3 with its features on a stable server OS.

I want my server environment to be fairly reproducible, with only configuration changes and not major application changes required.

## References
All git repo links above.

#!/bin/sh
# File: 2-upload-srpms.sh
# Author: bgstack15
# SPDX-License-Identifer: GPL-3.0
# Startdate: 2022-05-17 11:54
# Title: Upload SRPMs to copr
# Project: build-radicale-centos7
# Purpose: upload the generated srpms to copr in order
# History:
# Improve:
# Reference:
# Dependencies:
#    valid ~/.config/copr for copr authentication
# Documentation:
#    README.md

SCRIPTDIR="$( dirname "$( readlink -f "${0}" )" )"
. "${SCRIPTDIR}/lib.sh"
set -x
file_coverage=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "python-coverage*.src.rpm" )"
file_dateutil=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "python-dateutil*.src.rpm" )"
file_vobject=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "python-vobject*.src.rpm" )"
file_nose=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "python-nose*.src.rpm" )"
file_passlib=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "python-passlib*.src.rpm" )"
file_radicale=~/rpmbuild/SRPMS/"$( get_latest ~/rpmbuild/SRPMS "radicale*.src.rpm" )"

# By omitting the --nowait, we can cause the background jobs to delay so we
# operate in the correct order.
copr-cli build "${COPR_REPO}" "${file_coverage}" & job_coverage=$!
copr-cli build "${COPR_REPO}" "${file_dateutil}" & job_dateutil=$!
wait ${job_dateutil} && { copr-cli build "${COPR_REPO}" "${file_vobject}" & job_vobject=$! ; }
wait ${job_coverage} && { copr-cli build "${COPR_REPO}" "${file_nose}" & job_nose=$! ; }
wait ${job_nose} && { copr-cli build "${COPR_REPO}" "${file_passlib}" & job_passlib=$! ; }
wait ${job_passlib} ${job_vobject} && { copr-cli build "${COPR_REPO}" "${file_radicale}" & job_radicale=$! ; }

echo "DONE"

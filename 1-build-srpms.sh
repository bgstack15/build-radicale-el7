#!/bin/sh
# File: 1-build-srpms.sh
# Author: bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2022-05-17 10:20
# Title: Build SRPMs for radicale3 for el7
# Project: build-radicale-centos7
# Purpose: Build srpms for upload to copr
# History:
# Improve:
# Reference:
#    https://unix.stackexchange.com/questions/675833/how-can-i-duplicate-a-line-and-search-and-replace-on-the-duplicate-with-sed/675837#675837
# Dependencies:
#    git
#    Run this on CentOS 7
#    radicale-proxy-ldap-auth-fn.js.patch from this repo
# Documentation:
#    README.md

SCRIPTDIR="$( dirname "$(readlink -f "${0}" )" )"

. "${SCRIPTDIR}/lib.sh"

# If you actually compile these packages locally, you need to install these packages.
if test -n "${BUILD_PACKAGES}" ;
then
   need_packages=
   rpm -q python36-bcrypt 1>/dev/null || need_packages="${need_packages} python36-bcrypt"
   rpm -q python36-dateutil 1>/dev/null || need_packages="${need_packages} python36-dateutil"
   test -n "${need_packages}" && sudo yum install ${need_packages}
fi

mkdir -p "${WORKDIR}"
cd "${WORKDIR}"

# need python3-coverage
git clone https://src.fedoraproject.org/rpms/python-coverage python-coverage
( cd python-coverage ; git checkout -- *.spec ; git checkout "${FEDORA_VER}" ; cp -pf * ~/rpmbuild/SOURCES/ ; rpmbuild -bs *.spec )
# generated ~/rpmbuild/SRPMS/python-coverage*.src.rpm

# need python3-nose
git clone https://src.fedoraproject.org/rpms/python-nose python-nose
( cd python-nose ; git checkout -- *.spec ; git checkout "${FEDORA_VER}" ; cp -pf * ~/rpmbuild/SOURCES/ ; rpmbuild -bs *.spec )

# need python36-passlib
# This needs python36-bcrypt when compiling, for some inexplicable reason.
git clone https://src.fedoraproject.org/rpms/python-passlib python-passlib
(
   cd python-passlib
   git checkout -- *.spec ; git checkout "${FEDORA_VER}"
   sed -i -r python-passlib.spec \
      -e '/BuildRequires.*nose/aBuildRequires: python36-bcrypt'
   cp -pf * ~/rpmbuild/SOURCES/ ;
   rpmbuild -bs *.spec
)

# need a newer version of python36-dateutil than what CentOS 7 provides
git clone https://src.fedoraproject.org/rpms/python-dateutil python-dateutil
(
   cd python-dateutil
   git checkout -- *.spec ; git checkout "${FEDORA_VER}"
   # Disable tests. Fix some python3 to python36 names. Disable docs which are needlessly complicated.
   sed -i -r python-dateutil.spec \
      -e '/%bcond.*tests/s/_without/_with/' \
      -e '/^\w*Requires:.*(sphinx|setuptools|freezegun|hypothesis|pytest|six)/s/python3-/python36-/;' \
      -e '/docs.*html/{s/^/#/;s/%/%%/g;};' ;
   cp -pf * ~/rpmbuild/SOURCES/ ;
   rpmbuild -bs *.spec ;
)

# need python36-vobject to use adjusted dateutil package name, and also add buildrequires: python36-six
# This needs python36-dateutil when compiling
git clone https://src.fedoraproject.org/rpms/python-vobject python-vobject
(
   cd python-vobject ;
   git checkout -- *.spec ; git checkout "${FEDORA_VER}"
   sed -i -r python-vobject.spec \
      -e '/^\w*Requires:.*(dateutil)/s/python3-/python36-/;' \
      -e '/^Summary/aBuildRequires:      python36-six\' \
      -e 'Requires:       python36-six'
   cp -pf * ~/rpmbuild/SOURCES/ ;
   rpmbuild -bs *.spec ;
)

# and now prepare radicale3
git clone https://src.fedoraproject.org/rpms/radicale radicale
(
   cd radicale ;
   git checkout -- *.spec ; git checkout "${FEDORA_VER}"
   sed -i -r radicale.spec \
      -e '/^(\w*Requires(\(\w*\))?:|%package|%post|%files|#echo|#\/usr|%description|%\{\?python_provide|Conflicts|Suggests:|Recommends:)/{s/python3-/\%{pyver}-/g;}' \
      -e '/^sed/s/python3/%{pyver}/g;' \
      -e '/^SELinux|^httpd example/s/Python3/%{pyver}/;' \
      -e '/^\w*Requires(\(\w*\))?:/{s/policycoreutils-python-utils/policycoreutils-python/g;};' \
      -e 's/^(Suggests|Recommends):/Requires:/;' \
      -e '/%define.*radicale_major/a# stackrpms, 5\' \
         -e '%define pyver python3\' \
         -e '%if 0%{?centos}\' \
         -e '%define pyver python36\' \
         -e '%endif' \
      -e '/^%autosetup/s/$/ -p1/;' \
      -e '/Patch0:/aPatch1:           %{name}-proxy-ldap-auth-fn.js.patch' \
      -e '/BuildRequires:.*(defusedxml|passlib|dateutil|vobject)/{p;s/^BuildR/R/;}'
   cp -pf "${SCRIPTDIR}/"*.patch ~/rpmbuild/SOURCES/
   cp -pf * ~/rpmbuild/SOURCES/
   rpmbuild -bs *.spec ;
)

# Next steps: run script 2
